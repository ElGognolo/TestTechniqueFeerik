﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementIngame : MonoBehaviour {

    [SerializeField]
    int idTexture;
    [SerializeField]
    private string url;
    [SerializeField]
    private Material associatedMaterial;
    // Use this for initialization
    //En vérité il faudrait charger la texture avant l'activation effective de l'objet pour ne pas avoir de latence entre l'apparition del'objet non texturé et son texturing
    void OnEnable()
    {
        TextureLoader.GetInstance().LoadTexture(idTexture,url,this);
    }

    public void SetTexture(Texture _texture)
    {
        associatedMaterial.mainTexture = _texture;
        byte[] bytes = null;
        Texture2D texture = associatedMaterial.mainTexture as Texture2D;
        bytes = texture.EncodeToPNG();
        VRAMManager.GetInstance().SaveTexture(bytes, idTexture);
    }
    void OnDisable()
    {
        /*byte[] bytes = null;
        Texture2D texture=associatedMaterial.mainTexture as Texture2D;
        bytes = texture.EncodeToPNG();
        VRAMManager.GetInstance().SaveTexture(bytes, idTexture);*/
    }
}
