﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TextureLoader:MonoBehaviour{//obligé de mettre en monoBehaviour autrement on a pas accès a un lancement de coroutine mais je dois pouvoir changer cela sous peu avec un vrai threa C#
    private static TextureLoader instance;
    private Texture[] texture;
    [SerializeField]
    private Material material;
    private bool finishDownLoad = false;

    public static TextureLoader GetInstance()
    {
        if (instance != null)
        {
            return instance;
        }
        else
        {
            instance = GameObject.FindGameObjectWithTag("GameController").GetComponent<TextureLoader>();
            return instance;
        }
    }


	// Use this for initialization


	void Awake () {
        texture = new Texture[2];
        instance = this;
    }

    IEnumerator GetTextureFromWebCoroutine(string _url,int id,ElementIngame elt)
    {
        UnityWebRequest unityWebRequest= UnityWebRequestTexture.GetTexture(_url);
        yield return unityWebRequest.Send(); //il semble que je suis sur une ancienne version de Unity depuis la méthode est unityWebRequest.SendWebRequest

        if (unityWebRequest.isNetworkError || unityWebRequest.isHttpError)
        {
            Debug.Log(unityWebRequest.error);
        }
        else
        {
            Debug.Log("complete download");
            elt.SetTexture(((DownloadHandlerTexture)unityWebRequest.downloadHandler).texture);
            //texture[id] = ((DownloadHandlerTexture)unityWebRequest.downloadHandler).texture;
        }
        finishDownLoad = true;
    }


    public Texture GetTexture(int i)
    {
        return texture[i];
    }

    //utilisé pour tester si je récupère bien la texture que je veux
    public void FixedUpdate()
    {
        //material.mainTexture = texture ;
    }


    public void LoadTexture(int id,string _url,ElementIngame elt)
    {
        Texture2D texture2D = new Texture2D(0, 0);
        byte[] texture = VRAMManager.GetInstance().LoadTexture(id);

        if(texture == null)
        {
            StartCoroutine(GetTextureFromWebCoroutine(_url,id,elt));
            finishDownLoad = false;
            Debug.Log("setting the texture");
            //elt.SetTexture(this.texture[id]);
        }
        else
        {
            Debug.Log("setting the texture");
            texture2D.LoadImage(texture);
            elt.SetTexture(texture2D);
        }
    }



}