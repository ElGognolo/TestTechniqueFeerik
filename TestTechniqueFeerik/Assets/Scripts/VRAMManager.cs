﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.IO;

public class VRAMManager:MonoBehaviour{//MonoBehaviourpour paramétrer les thread dans l'interface Unity
    //On admet que l'on sait identifier de façon unique les texture utilisées pour le jeu à l'aide d'un entier identifiant qui pourra être remplacé par un système d'enums si nécessaire pour rendre le code plus lisible
    // Use this for initialization
    [SerializeField]
    private string pathToFolder;
    [SerializeField]
    private int nbThreads;
    [SerializeField]
    private byte[][] bytes;
    private bool[] threadsDispo;
    private static VRAMManager instance;
    public static VRAMManager GetInstance()
    {
        if (instance != null)
        {
            return instance;
        }
        else
        {
            instance = GameObject.FindGameObjectWithTag("GameController").GetComponent<VRAMManager>();
            return instance;
        }
       
    }
    void Awake () {
        Debug.Log("init des threadsDispo");
        threadsDispo = new bool[nbThreads];
        for(int i = 0; i < nbThreads; i++)
        {
            threadsDispo[i] = true;
        }
        bytes = new byte[nbThreads][];
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public byte[] LoadTexture(int identifier)
    {
        int i;
        i = ThreadDispo();

        Texture2D texture2D = new Texture2D(0, 0);
        Thread thread = new Thread(() => LoadTextureThread(identifier,i,texture2D));
        thread.Start();
        while (thread.IsAlive)
        {

        }
        return bytes[i];

    }
    //renvoie la texture si elle est en mémoire, null sinon
    public void LoadTextureThread(int identifier,int _i, Texture2D texture2D)
    {
        threadsDispo[_i] = false;
        Debug.Log("Im starting and Im thread n° " + _i+" loading a texture");
        string path = pathToFolder + "/" + identifier + ".png";//j'obtient une erreur me disant que l'acces a la lecture ne peut se faire que depuis le thread principal
        Texture texture=null;
        byte[] _bytes;
        if (File.Exists(path))
        {
            _bytes = File.ReadAllBytes(path);
            //texture2D.LoadImage(bytes);
            //insérer le code qui est capable d'aller chercher la texture dans la mémoire et la stocke dans la variable texture;
            //si celle ci ne s'y trouve pas on renvoie null
            threadsDispo[_i] = true;
            bytes[_i]=_bytes;
        }
        else
        {
            threadsDispo[_i] = true;
            bytes[_i]=null;
        }
        return;
    }


    public void SaveTexture(byte[] bytesToStore,int identifier)
    {
        int i;
        i = ThreadDispo();

        Thread thread = new Thread(() => SaveTextureThread(bytesToStore,identifier, i));
        thread.Start();
    }
    public void SaveTextureThread(byte[] bytesToStore, int identifier,int _i)
    {
        threadsDispo[_i] = false;
        Debug.Log("Im starting and Im thread n° " + _i+" saving a texture");
        string path = pathToFolder + "/" + identifier + ".png";
        File.WriteAllBytes(path, bytesToStore);
        threadsDispo[_i] = true;
    }

    public int ThreadDispo()
    {
        while (true)
        {
            for (int i = 0; i < nbThreads; i++)
            {
                //Debug.Log(threadsDispo);
                Debug.Log(threadsDispo[i]);
                if (threadsDispo[i])
                {
                    return i;
                }
            }
        }
    }

}
